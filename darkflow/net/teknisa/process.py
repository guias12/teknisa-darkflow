import os
import traceback
import sys
import time
import pika
import json
import cv2
import datetime
import base64
import numpy as np
from termcolor import colored
from botocore.config import Config
from decouple import config
from .core import frameAnalyzer
from .core.SaleItemAnalyzer import SaleItemAnalyzer
from .core.LayoutController import LayoutController
from .core.SaleController import SaleController
from .util.logger import Logger
from .queue import filaVendas
from .queue import enviarMensagem
from .util import hasNextWrapper

RABBIT_MQ_HOST = config('RABBIT_MQ_HOST')
RABBIT_MQ_PORT = config('RABBIT_MQ_PORT')
MODELO_EXECUCAO = int(config('MODELO_EXECUCAO'))
VERIFICA_POSICAO_BANDEJA = config('VERIFICA_POSICAO_BANDEJA')

positions = []
lastTime = 0
oldObjects = []
layoutController = LayoutController()
saleAnalyzer = SaleItemAnalyzer()
logger = Logger()
frame = None
framesCorretosConferencia = 0

def setFrame(frameCV):
    global frame

    frame = frameCV


#imported methods
#searchProduct = product.searchProduct
limparFrames = frameAnalyzer.limparFrames
liberarRecebimentoVendas = SaleItemAnalyzer.liberarRecebimentoVendas

def encode(frame):
    encode_param = [int(cv2.IMWRITE_JPEG_QUALITY), 45]
    retval, buffer = cv2.imencode('.jpg', frame, encode_param)
    jpg_as_text = str(base64.b64encode(buffer))
    return jpg_as_text    

def processJson(itens):
    global oldObjects
    global saleAnalyzer
    global layoutController
    saleController = SaleController()
    objetos = []

    if (MODELO_EXECUCAO == 1):

        bandejaCorreta = verificarBandeja()

        if (bandejaCorreta):
            if len(itens) == 0:
                layoutController.updateStatus(True)
                saleAnalyzer.liberarRecebimentoVendas()
                frameAnalyzer.limparFrames()
            else:
                if layoutController.permiteMudancaLayout:
                    filaVendas.enviarMudancaLayout()            
                frameAnalyzer.addFrame(itens)
                objetos = frameAnalyzer.getItemsList()
                layoutController.updateStatus(False)

            frames = frameAnalyzer.getFrames()
            
            if objetos is not None:
                saleController.analizar(frames, objetos)
                if saleController.autorizarEnvio and not saleAnalyzer.podeReceberNovaVenda:                    
                    filaVendas.enviarVenda(objetos, saleAnalyzer)

    elif (MODELO_EXECUCAO == 2):

        if (verificarBandeja()):
            dados = {
                "action": "products",
                "data": itens,
                "datetime": str(datetime.datetime.now())
            }

            frameAnalyzer.addFrame(itens)
            objetos = frameAnalyzer.getItemsList()

            if (len(oldObjects) != len(objetos)):
                logger.registrarLog()
                enviarMensagem.enviarMensagem('odhengo_channel', json.dumps(dados))
                oldObjects = objetos
            
     

def verificarBandeja():

    global VERIFICA_POSICAO_BANDEJA
    global frame
    global framesCorretosConferencia

    resultado = True

    if (str_to_bool(VERIFICA_POSICAO_BANDEJA)):
        font = cv2.FONT_HERSHEY_PLAIN

        #cropped = frame[15:40, 90:620]
        cropped = frame[0:50, 0:640]

        blurred_frame = cv2.GaussianBlur(cropped, (5, 5), 0)
        hsv = cv2.cvtColor(blurred_frame, cv2.COLOR_BGR2HSV)

        lower_blue = np.array([54, 80, 46])
        upper_blue = np.array([85, 195, 255])
        
        '''
        lower_blue = np.array([38, 70, 20])
        upper_blue = np.array([85, 195, 255])
        '''
        
        mask = cv2.inRange(hsv, lower_blue, upper_blue)

        contours = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
            
        if (len(contours) != 1):
            resultado = False
        else:
            if (framesCorretosConferencia < 3):
                framesCorretosConferencia = framesCorretosConferencia + 1
                resultado = False
            else:
                resultado = True
                framesCorretosConferencia = 0
           # resultado = False
           # framesCorretosConferencia = 0
                
        #for contour in contours:
        #    cv2.drawContours(frame, contour, -1, (0, 255, 0), 3)

        #cv2.putText(frame, "Contornos: " + str(len(contours)), (50, 50), font, 2, (255, 0, 0), 3)

    return resultado

def str_to_bool(s):
    if s == 'True':
         return True
    elif s == 'False':
         return False