import threading
import pika
import time
import json
from botocore.config import Config
from decouple import config
from .core import frameAnalyzer
from .core.SaleItemAnalyzer import SaleItemAnalyzer
from .integration import criarGoomer
from .integration import enviarVenda

SERVER_CLIENT_QUEUE_NAME = config('SERVER_CLIENT_QUEUE_NAME')
CLIENT_SERVER_QUEUE_NAME = config('CLIENT_SERVER_QUEUE_NAME')
RABBIT_MQ_HOST = config('RABBIT_MQ_HOST')
RABBIT_MQ_PORT = config('RABBIT_MQ_PORT')

class FilaServerServer:

    def __init__(self):
        self.compraConfirmada = False

    def receberMensagens(self):
        global RABBIT_MQ_HOST
        global RABBIT_MQ_PORT
        global SERVER_CLIENT_QUEUE_NAME
        global CLIENT_SERVER_QUEUE_NAME

        saleAnalyzer = SaleItemAnalyzer()

        connection = pika.BlockingConnection(pika.ConnectionParameters(host = RABBIT_MQ_HOST, port = RABBIT_MQ_PORT))
        channel = connection.channel()
        channel.queue_declare(queue = CLIENT_SERVER_QUEUE_NAME)


        def callback(ch, method, properties, body):
            body = body.decode("utf-8")
            msg = json.loads(body)
            
            if msg['action'] == 'confirm':
                frameAnalyzer.limparFrames()
                self.compraConfirmada = True
                saleAnalyzer.limparVendas()
                jsonVenda = criarGoomer.criarJSON(msg['data'])
                enviarVenda.enviarVenda(jsonVenda)           


        channel.basic_consume(queue = CLIENT_SERVER_QUEUE_NAME, on_message_callback = callback, auto_ack = True)

        print(' [*] Waiting for messages. To exit press CTRL+C')
        channel.start_consuming()


    def main(self):    
        t1 = threading.Thread(target=self.receberMensagens, daemon=True )
        t1.start()