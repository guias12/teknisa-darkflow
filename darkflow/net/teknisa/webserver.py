import threading
from flask import Flask, render_template, Response
import cv2
from botocore.config import Config
from decouple import config

app = Flask(__name__)

HTTP_HOST = config('HTTP_HOST')
HTTP_PORT = config('HTTP_PORT')

image = None

class WebServer:

    def __init__(self):
        global image
        print(" [*] Starting WebServer.")

    def startServer(self):
        global HTTP_HOST
        global HTTP_PORT

        app.run(host=HTTP_HOST, debug=False, port=HTTP_PORT)

    @app.route('/')
    def index():
        url = '127.0.0.1'
        if (HTTP_HOST != '0.0.0.0'):
            url = HTTP_HOST
        url = 'http://' + url + HTTP_PORT 
        
        return render_template('index.html', url=url)

    def setImage(self, pimage):
        global image
        image = pimage

    @app.route('/video_feed')
    def video_feed():
        global image

        def gen():
            try:
                while True:
                    frame = get_frame()
                    yield (b'--frame\r\n'
                        b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')
            except:
                print("Error gen.")

        def get_frame():
            try:
                ret, jpeg = cv2.imencode('.jpg', image)
                return jpeg.tobytes()
            except:
                print("Error get_frame.")

        return Response(gen(), mimetype='multipart/x-mixed-replace; boundary=frame')

        
    @app.route('/verificar')
    def verificar():
        result = "{ 'status': 'online' }"
        return result

    def main(self):    
        t1 = threading.Thread(target=self.startServer, daemon=True )
        t1.start()