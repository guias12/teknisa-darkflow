import os
import traceback
import sys
import time
import json
from . import enviarMensagem
from ..core import frameAnalyzer
from ..components.product import Product
from botocore.config import Config
from decouple import config

SERVER_CLIENT_QUEUE_NAME = config('SERVER_CLIENT_QUEUE_NAME')
CLIENT_SERVER_QUEUE_NAME = config('CLIENT_SERVER_QUEUE_NAME')
produtos = Product()

def enviarLiberacao(saleAnalyzer):
	msg = {
		"action": "available",
		"data": ""
	}
	jsonMsg = json.dumps(msg)
	enviarMensagem.enviarMensagem(SERVER_CLIENT_QUEUE_NAME, jsonMsg)
	saleAnalyzer.updateCaixaAberto(False)

def enviarVenda(objetos, saleAnalyzer):
	listaItens = []	
	global produtos
	#removendo itens com a propriedade label repetida	
	listaObjetos = list(dict((v['label'],v) for v in objetos).values())

	for objeto in listaObjetos:		
		listaItens.append(produtos.searchProduct(objeto['label']))

	msg = {
		"action": "addItem",
		"data": listaItens
	}

	jsonMsg = json.dumps(msg)
	
	enviarMensagem.enviarMensagem(SERVER_CLIENT_QUEUE_NAME, jsonMsg)
	frameAnalyzer.limparFrames()
	saleAnalyzer.updateStatusCaixa(True)
	saleAnalyzer.updateCaixaAberto(True)

def enviarMudancaLayout(sendOnce = []):	
	msg = {
		"action": "changeLayout",
		"data": ""
	}

	jsonMsg = json.dumps(msg)

	enviarMensagem.enviarMensagem(SERVER_CLIENT_QUEUE_NAME, jsonMsg)
	sendOnce.append('Not Empty')
	