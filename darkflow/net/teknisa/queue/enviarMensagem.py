import os
import traceback
import sys
import time
import pika
import json
import cv2
import base64

from botocore.config import Config
from decouple import config


RABBIT_MQ_HOST = config('RABBIT_MQ_HOST')
RABBIT_MQ_PORT = config('RABBIT_MQ_PORT')

def enviarMensagem(fila, mensagem):

    global RABBIT_MQ_HOST
    global RABBIT_MQ_PORT

    connection = pika.BlockingConnection(pika.ConnectionParameters(host = RABBIT_MQ_HOST, port = RABBIT_MQ_PORT))
    channel = connection.channel()

    channel.queue_declare(queue = fila)

    channel.basic_publish(exchange = '', routing_key = fila, body = mensagem)
    connection.close()