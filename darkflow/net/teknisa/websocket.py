import threading
import asyncio
import websockets

host = 'localhost'
port = 5672

class WebSocket:

    def __init__(self):
        print(" [*] Starting WS")

    @asyncio.coroutine
    def handle(websocket, path):
        print ("Aquiiii")
        name = yield from websocket.recv()
        print(f"< {name}")

        greeting = f"Hello {name}!"

        yield from websocket.send(greeting)
        print(f"> {greeting}")
    
    @asyncio.coroutine
    def send(self, message):
        global host
        global port

        websocket = yield from websockets.connect('ws://'+host+':'+port)

        try:
            yield from websocket.send(message)
        finally:
            yield from websocket.close()

    def main(self): 
        start_server = websockets.serve(self.handle, host, port)
        asyncio.get_event_loop().run_until_complete(start_server)

        t1 = threading.Thread(target=asyncio.get_event_loop().run_forever, daemon=True )
        t1.start()