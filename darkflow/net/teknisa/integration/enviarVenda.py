import requests
from termcolor import colored
from datetime import datetime
from botocore.config import Config
from decouple import config

BASE_API_VENDAS= config('BASE_API_VENDAS')

def enviarVenda(jsonVenda):
	global BASE_API_VENDAS	
	urlApiVendas = BASE_API_VENDAS + '/sale/saveDeliverySale'
	r = requests.post(urlApiVendas, jsonVenda)
	horaVenda = datetime.now().strftime('%d/%m/%Y %H:%M:%S')
	if r.status_code == 200:
		mensagemSucesso = 'Venda enviada com sucesso!!! ' + str(horaVenda)
		print(colored(mensagemSucesso, 'blue'))
	else:
		print(colored(('Erro ao enviar a venda', r.reason), 'blue'))