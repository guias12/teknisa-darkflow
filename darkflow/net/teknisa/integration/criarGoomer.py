import time
import json

def criarJSON(produtos):
	valorTotal = 0
	items = []	
	for produto in produtos:
		item = {}
		valorTotal += produto['price']
		item["unitValue"] = produto['price']
		item["productCode"] = produto['code']
		item["productName"] = produto['name']
		item["quantity"] = produto['qt']
		item["discount"] = 0
		item["observations"] = {}
		item["childItems"] = {}
		items.append(item)

	payments = []
	payment = {}
	payment["paymentCode"] = "001"
	payment["paymentName"] = "DINHEIRO"
	payment["value"] = valorTotal
	payment["nsu"] = "260004"

	payments.append(payment)

	orderDescription = {}
	orderDescription["toTravel"] = False
	orderDescription["orderType"] = "TAA"
	orderDescription["productsValue"] = valorTotal
	orderDescription["deliveryFee"] = 0
	orderDescription["orderDiscount"] = 0 
	orderDescription["totalValue"] = valorTotal
	orderDescription["description"] = ""
	orderDescription["orderCPF"] = ""
	orderDescription["changeValue"] = 0 
	orderDescription["serviceTax"] = 0
	orderDescription["payments"] = payments
	orderDescription["deliveryAddress"] = None
	orderDescription["isScheduled" ] = False

	order = {}
	order["subsidiaryCode"] = "9999"
	order["storeCode"] = "01"
	order["posCode"] = "010"
	order["pagerId"] = None
	order["tableNumber"] = None
	order["sellerCode"] = None
	order["items"] = items
	order["clientCode"] = "O00001"
	order["consumerCode"] = "000000000000000000001"
	order["consumerId"] = "CONSUMIDOR PADRAO"
	order["orderDescription"] = orderDescription

	jsonOrder = {}
	jsonOrder["order"] = order

	finalJson = json.dumps(jsonOrder)	

	return finalJson
	


