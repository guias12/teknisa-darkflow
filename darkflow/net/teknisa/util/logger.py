import cv2
from datetime import date
from botocore.config import Config
from decouple import config
from termcolor import colored
import os
import uuid
import json, codecs

LOG_PATH = config('LOG_PATH')
LOG_HABILITADO = config('LOG')

class Logger:

    def __init__(self):
        self.podeAbrirCaixa = True   

    def criarPath(self): 
        today = date.today()
        fullPath = LOG_PATH+today.strftime("/%Y/%m/%d/")+uuid.uuid4().hex

        return fullPath

    def criarPastaLog(self):
        today = date.today()
        
        ano = today.strftime("%Y")
        pathAno = LOG_PATH+'/'+ano

        if not os.path.exists(pathAno):
            os.makedirs(pathAno)
        
        mes = today.strftime("%m")
        pathMes = pathAno+'/'+mes
        
        if not os.path.exists(pathMes):
            os.makedirs(pathMes)
        
        dia = today.strftime("%d")
        pathDia = pathAno+'/'+dia
        
        if not os.path.exists(pathDia):
            os.makedirs(pathDia)

    def registrarLog(self):

        if(str_to_bool(LOG)):

            try:
                path = self.criarPath()
                self.criarPastaLog()

                data = {'price': '€10'}

                with open(path, 'wb') as f:
                    json.dump(data, codecs.getwriter('utf-8')(f), ensure_ascii=False)

            except Exception as e:
                print("Erro ao criar o log")
                print(str(e))
            

        
    def str_to_bool(s):
        if s == 'True':
            return True
        elif s == 'False':
            return False