import pyodbc 
import os
import requests
import json
from dotenv import load_dotenv
from datetime import datetime

load_dotenv()
products = { "data": []}
API_URL = os.getenv("BASE_API_VENDAS")

def fetchProdcts():
    dbServer = os.getenv("SERVER_NAME")
    dbName = os.getenv("DATABASE_NAME")

    config = "Driver={}; Server={}; Database={}; Trusted_Connection=yes"
    config = config.format("{SQL Server}", dbServer, dbName)

    global products

    conn = pyodbc.connect(config)
    cursor = conn.cursor()
    query = "SELECT DSLABELPROD, CDPRODUTO, NMPRODUTO, CDNVPRODUTO FROM {}.PRODUTO WHERE CDNVPRODUTO IN (SELECT MAX(CDNVPRODUTO) FROM {}.PRODUTO) AND DSLABELPROD IS NOT NULL"
    query = query.format(dbName, dbName)
    cursor.execute(query)    

    for row in cursor:    
        product = {}
        info = {}
        product[row[0]] = info
        info["label"] = row[0]
        info["code"] = row[1]
        info["name"] = row[2]
        info["price"] = getItemPrice(row[1])
        info["qt"] = 1        
        addProduto(product)
    
    formatProductsObj()    

    return products

def addProduto(product):
    global products
    products["data"].append(product)
    

def formatProductsObj():
    global products
    prodAux = {}
    
    for product in products["data"]:
        for key, val in product.items():        
            prodAux[key] = val
            
    
    products = prodAux
    
    
def getItemPrice(code):
    global API_URL    
    data = {}

    horario = "{}{}"
    now = datetime.now()    
    horario = horario.format(now.hour, now.minute)
    endpoint = API_URL + "/menu/getByPos"

    """
        É preciso que esses parâmetros sejam buscados no banco ao invés de serem fixados,
        precisamos avaliar uma regra melhor para obtê-los.

    """
    data["subsidiaryCode"] = os.getenv("CDFILIAL")
    data["storeCode"] = os.getenv("CDLOJA")
    data["posCode"] = os.getenv("CDCAIXA")
    data["consumerType"] = "T"    
    data["hour"] = horario
    
    #Fazendo a requisição para a integração(Odhen Interface) no endpoint "getByPos"
    r = requests.post(endpoint, json = data)
    response = json.loads(r.text)
    
    
    if r.status_code == 200:
        for grupoDeProduto in response["data"]:
            for produto in grupoDeProduto["products"]:
                if code == produto["productCode"]:
                    return produto["price"]
    
    