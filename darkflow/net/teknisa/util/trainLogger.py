import os
import traceback
import __root__
from datetime import datetime

def createLogFile():
    logsPath = (os.path.join(__root__.path(), 'trainLogs'))
    now = datetime.now()
    logFileName = '{}-{}-{} {}-{}-{}.txt'.format(
        now.day, now.month, now.year, now.hour, now.minute, now.second)
    

    logFile = (os.path.join(logsPath, logFileName))

    if not os.path.isdir(logsPath):
        os.mkdir(logsPath)
    
    return logFile


def writeDefaultLog(error):
    directory = createLogFile()

    traceback_str = ''.join(traceback.format_tb(error.__traceback__))

    fullErrorStr = str(error) + '\n\n' + str(traceback_str)

    if not os.path.isfile(directory):
        with open(directory, 'w') as tempTxt:
            tempTxt.write(str(fullErrorStr))


def writeAnnLog(ann):
    directory = createLogFile()

    srtError = "Treinamento Interrompido \nDiretório da anotação {} não encontrado"

    with open(directory, 'w') as tempTxt:
        tempTxt.write(srtError.format(ann))


def writeZeroDivLog(train_instance, error):
    directory = createLogFile()

    logMsg = "A largura ou comprimento da imagem {} é zero\nVerifique seu arquivo e sua respectiva anotação e tente novamente \n\ntrain_instance: {}"

    traceback_str = ''.join(traceback.format_tb(error.__traceback__))

    logMsg = logMsg + '\n\n' + str(traceback_str)    

    with open(directory, 'w') as tempTxt:
        tempTxt.write(str(logMsg.format(train_instance[0], train_instance)))
    

def writeAttributeLog(train_instance, error):
    directory = createLogFile()

    logMsg = "Algum erro ocorreu com a imagem: {}, verifique seu arquivo e sua respectiva anotação e tente novamente \n\ntrain_instance: {}"
    logMsg = logMsg.format(train_instance[0], train_instance)

    traceback_str = ''.join(traceback.format_tb(error.__traceback__))

    logMsg = logMsg + '\n\n' + str(traceback_str)    

    with open(directory, 'w') as tempTxt:
        tempTxt.write(str(logMsg))
