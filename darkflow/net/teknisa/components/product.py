from termcolor import colored
from ..util import db

class Product:  

    def __init__(self, code = '', name = '', price = 0):
        self.productCode = code
        self.productName = name
        self.productPrice = float(price)
        self.products = db.fetchProdcts()

    def getProductName(self):
        return self.productName

    def getProductCode(self):
        return self.productCode

    def getProductPrice(self):
        return self.productPrice

    def searchProduct(self, search):
        retorno = None     

        if search == 'fanta':
            search = 'fanta_lata_350'
        
        if search in self.products:            
            
            retorno = self.products.get(search)
            print(colored(('Produto adicionado:', retorno['name']), 'red'))

        return retorno
            