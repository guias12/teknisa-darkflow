import os
import traceback
import sys
import time
import json
from ..queue import filaVendas

class LayoutController:

	def __init__(self):
		self.permiteMudancaLayout = True

	def updateStatus(self, status):
		self.permiteMudancaLayout = status
	
	def main(self):    
		t1 = threading.Thread(target = self.startServer, daemon = True )
		t1.start()