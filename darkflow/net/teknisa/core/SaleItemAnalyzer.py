import os
import traceback
import sys
import time
import json
from ..queue import filaVendas

class SaleItemAnalyzer:

	def __init__(self):
		self.podeReceberNovaVenda = False
		self.caixaAberto = True

	def liberarRecebimentoVendas(self):
		if self.caixaAberto:
			self.podeReceberNovaVenda = False		
			filaVendas.enviarLiberacao(self)

	def updateStatusCaixa(self, status):
		self.podeReceberNovaVenda = status

	def updateCaixaAberto(self, status):
		self.caixaAberto = status

	def limparVendas(self):
		self.podeReceberNovaVenda = False

	def main(self):    
		t1 = threading.Thread(target = self.startServer, daemon = True )
		t1.start()