import os
import traceback
import sys
import time
import json
from termcolor import colored

QTD_FRAMES_ANALYTICS = 30
MAX_DIFF = 15
MIN_FRAMES_CONSIDERATION = 5
MIN_CONFIDENCE = 0.3
frames = []
stable = True
containedObjects = []

def addFrame(frame):
	global frames	
	global QTD_FRAMES_ANALYTICS
	if (len(frames) + 1) > QTD_FRAMES_ANALYTICS:
		frames.pop(0)

	frames.append(frame)
	

def analyze():
	global frames
	global MIN_FRAMES_CONSIDERATION
	global MIN_CONFIDENCE
	global stable	
	if len(frames) > MIN_FRAMES_CONSIDERATION:
		frame = frames[0]		
		for item in frame:			
			if len(getFrameObjects(item['label'])) > MIN_FRAMES_CONSIDERATION and item['confidence'] > MIN_CONFIDENCE:
				recentAvg = getAvgPositionsRange(0, 5, item['label'])
				olderAvg = getAvgPositionsRange(0, 5, item['label'])
				
				diffX = recentAvg[0] - olderAvg[0]
				diffY = recentAvg[1] - olderAvg[1]				

				if(abs(diffX) > MAX_DIFF or abs(diffY) > MAX_DIFF):
					stable = False
	print(stable)							


def getFrameObjects(label):
	global frames
	containedFrames = []

	if len(frames) > 0:
		for item in frames:
			i = len(item)			
			while i > 0:
				objeto = item[i - 1]
				
				if objeto['label'] == label:
					containedFrames.append(objeto)
					i = 0

				i -= 1
	
	return containedFrames


def getAvgPositionsRange(start, itens, label):
	global frames
	i = 0
	sumPositionX = 0
	sumPositionY = 0
	totalItens = 0
	totalFrames = len(frames)
	avgX = 0
	avgY = 0
	avg = []

	while i < itens:
		if (start + i) < totalFrames:
			objects = frames[start + 1]
			size = len(objects)

			for objeto in objects:
				if objeto['label'] == label:
					topLeftX = objeto['topleft']['x']
					topLeftY = objeto['topleft']['y']

					sumPositionX += topLeftX
					sumPositionY += topLeftY

					totalItens += 1


		i += 1
	

	if totalItens > 0:
		avgX = sumPositionX / totalItens
		avgY = sumPositionY / totalItens				

	avg = [avgX, avgY]
	

	return avg


def limparFrames():
	global frames
	global containedObjects
	frames = []
	containedObjects = []

def getItemsList():
	global frames
	global containedObjects
	global QTD_FRAMES_ANALYTICS
	
	if len(frames) < QTD_FRAMES_ANALYTICS:
		pass
	else:
		frame = frames[QTD_FRAMES_ANALYTICS-1]
		i = 0
		while i < len(frame):		
			containedObjects.append(frame[i])
			i += 1			
		return containedObjects

def getFrames():
	global frames
	return frames

def objetosEquals(objetos):
	global frames
	global QTD_FRAMES_ANALYTICS

	for objeto in objetos:
		for item in frame:
			if (item['label'] == objeto['label']):
				possuiObjeto = True


