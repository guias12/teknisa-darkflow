import threading
import cv2
import time
from ..queue import filaVendas

class CaixaController:

    def __init__(self):
        self.podeAbrirCaixa = True    

    def updateAbrirCaixa(self, status):
        self.podeAbrirCaixa = status

    def abrirCaixa(self):
        filaVendas.enviarLiberacao()
        self.podeAbrirCaixa = False

    def main(self):    
        t1 = threading.Thread(target = self.startServer, daemon = True )
        t1.start()