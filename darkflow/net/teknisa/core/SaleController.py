import threading
import cv2
import time
from .SaleItemAnalyzer import SaleItemAnalyzer

class SaleController:

    def __init__(self):
        self.autorizarEnvio = False      

    def enviarMensagem(self, objetos):
        self.autorizarEnvio = False    

    def analizar(self, frames, objetos):
        saleAnalyzer = SaleItemAnalyzer()
        listaObjetos = list(dict((v['label'],v) for v in objetos).values())        
        if len(frames) >= 30:
            if len(listaObjetos) == len(frames[29]):
                #Envio autorizado, caixa ocupado
                self.autorizarEnvio = True                    


    def main(self):    
        t1 = threading.Thread(target = self.startServer, daemon = True )
        t1.start()