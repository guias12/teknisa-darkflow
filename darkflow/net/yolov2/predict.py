import numpy as np
import math
import cv2
import os
import json
import time
import base64
import time
import traceback
from PIL import ImageFont, ImageDraw, Image
from ...utils.box import BoundBox
from ...cython_utils.cy_yolo2_findboxes import box_constructor
from ...net.teknisa import process

def expit(x):
	return 1. / (1. + np.exp(-x))

def _softmax(x):
    e_x = np.exp(x - np.max(x))
    out = e_x / e_x.sum()
    return out

def findboxes(self, net_out):
	# meta
	meta = self.meta
	boxes = list()
	boxes=box_constructor(meta,net_out)
	return boxes

def postprocess(self, net_out, im, save = False):
	"""
	Takes net output, draw net_out, save to disk
	"""

	boxes = self.findboxes(net_out)

	# meta
	meta 	  = self.meta
	threshold = meta['thresh']
	colors 	  = meta['colors']
	labels    = meta['labels']
	if type(im) is not np.ndarray:
		imgcv = cv2.imread(im)
	else: imgcv = im
	h, w, _ = imgcv.shape	

	resultsForJSON = []

	for b in boxes:
		boxResults = self.process_box(b, h, w, threshold)
		if boxResults is None:
			continue			

		left, right, top, bot, mess, max_indx, confidence = boxResults
		thick = int((h + w) // 300)

		cv2.rectangle(imgcv, (left, top), (right, bot), colors[max_indx], thick)
		cv2.putText(imgcv, mess, (left, top - 12), 0, 1e-3 * h, colors[max_indx],thick//3)

		resultsForJSON.append({"label": mess, "confidence": float('%.2f' % confidence), "topleft": {"x": left, "y": top}, "bottomright": {"x": right, "y": bot}})

	
	try:
		process.setFrame(imgcv)
		process.processJson(resultsForJSON)
	except:
		traceback.print_exc()		


	if not save: return imgcv

	cv2.imwrite(img_name, imgcv)