import os

imdir = 'Images2'
if not os.path.isdir(imdir):
    os.mkdir(imdir)

coca_folders = [folder for folder in os.listdir('.') if 'KSL' in folder]

n = 1834
for folder in coca_folders:
    for imfile in os.scandir(folder):
        os.rename(imfile.path, os.path.join(imdir, '{:06}.jpg'.format(n)))
        n += 1