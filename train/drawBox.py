import os
#pip install matplotlib
import matplotlib.pyplot as plt 
import cv2
from matplotlib.widgets import RectangleSelector
from generateXML import writeXML

#constantes globais
img = None
tlList = []
brList = []
objectList = []

#constantes
imageFolder = 'Images'
saveDir = 'Annotations'
obj = 'coca-cola'


'''
lineSelectorCallback:

Essa função irá receber as coordeandas da imagem no momento
do click(inicial e final) e gerar uma tupla com as informações
nas variaveis tlList(coordenadas top-left) e brList(coordenadas bottom-right)

@param click: representa as informações do primeiro click do usuário
@param release: representa as informações do ultimo click
'''

def lineSelectCallback(click, realease):
	global tlList
	global brList
	tlList.append((int(click.xdata), int(click.ydata)))
	brList.append((int(realease.xdata), int(realease.ydata)))
	objectList.append(obj);

'''
onKeyPress:

Essa função irá receber o disparo de evento e acionar a função
que irá escrever o arquivo XML com as informações para o treinamento

@param event: representa o disparo de evento
'''

def onKeyPress(event):
	global img
	global tlList
	global brList
	global objectList

	if event.key == 'q':		
		writeXML(imageFolder, img, objectList, tlList, brList, saveDir)
		tlList = []
		brList = []
		img = None
		objectList = []
		


def toggleSelector(event):
	toggleSelector.RS.set_active(True)


if __name__ == '__main__':
	for n, imageFile in enumerate(os.scandir(imageFolder)):
		img = imageFile
		fig, ax = plt.subplots(1)
		#leitura do path da imagem
		image = cv2.imread(imageFile.path)
		#conversão para o padrão RGB
		image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
		ax.imshow(image)
		toggleSelector.RS = RectangleSelector(
			ax,
			lineSelectCallback,
			drawtype = 'box',
			useblit = True,
			button = [1],
			minspanx = 5,
			minspany = 5,
			spancoords = 'pixels',
			interactive = True
		)
		#chamada das funções de callback		
		bbox = plt.connect('key_press_event', toggleSelector)
		key  = plt.connect('key_press_event', onKeyPress)
		plt.show()
		plt.close(fig)