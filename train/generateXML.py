import os
import cv2
#pip install lxml
from lxml import etree
import xml.etree.cElementTree as ET 
'''
writeXML: Função para escrever em um arquivo XML todas as instruções
necessárias para um treinamento.

@param folder: diretório de busca das imagens
@param img: Inforações das imagens, como filename e path
@param objects: Lista de objetos que foram selecionados e marcados com caixa
@param tl: Coordenadas top-left
@param br: Coordenadas bottom-right
@param savedir: Diretório onde o XML será salvo
'''

def writeXML(folder, img, objects, tl, br, savedir):
	if not os.path.isdir(savedir):
		os.mkdir(savedir)

	image = cv2.imread(img.path)
	height, width, depth = image.shape

	annotation = ET.Element('annotation')
	ET.SubElement(annotation, 'folder').text = folder
	ET.SubElement(annotation, 'filename').text = img.name
	ET.SubElement(annotation, 'segmented').text = '0'
	size = ET.SubElement(annotation, 'size')
	ET.SubElement(size, 'width').text = str(width)
	ET.SubElement(size, 'height').text = str(height)
	ET.SubElement(size, 'depth').text = str(depth)
	for obj, topl, botr in zip(objects, tl, br):
		ob = ET.SubElement(annotation, 'object')
		ET.SubElement(ob, 'name').text = obj
		ET.SubElement(ob, 'pose').text = 'Unspecified'
		ET.SubElement(ob, 'truncated').text = '0'
		ET.SubElement(ob, 'difficult').text = '0'
		bbox = ET.SubElement(ob, 'bndbox')
		ET.SubElement(bbox, 'xmin').text = str(topl[0])
		ET.SubElement(bbox, 'ymin').text = str(topl[1])
		ET.SubElement(bbox, 'xmax').text = str(botr[0])
		ET.SubElement(bbox, 'ymax').text = str(botr[1])

	xmlString = ET.tostring(annotation)
	root = etree.fromstring(xmlString)
	xmlString = etree.tostring(root, pretty_print=True)

	savePath = os.path.join(savedir, img.name.replace('png', 'xml'))

	with open(savePath, 'wb') as tempXml:
		tempXml.write(xmlString)


if __name__ == '__main__':
	img = [im for im in os.scandir('images') if 'pic_001' in im.name][0]
	xml_str = writeXML('images', img, ['apple'], [(10, 10)], [(100, 100)], 'annotations')
